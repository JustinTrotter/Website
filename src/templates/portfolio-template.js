import React from "react"

export default () => {
  return(
    <section>
      <div>
      <iframe
        title="test"
        id="unity-iframe"
        src="../../unity/index.html"
        width="1280"
        height="720"
      />
      </div>
    </section>
  )
}